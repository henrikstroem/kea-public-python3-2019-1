import random
import math

import numpy as np

from timing import timing


numbers = [random.random() for i in range(10_000_000)]

"""Naive implementation"""
@timing
def run():
    cos_numbers = []
    for x in numbers:
        cos_numbers.append(math.cos(x))

run()


"""Using a listcomprehension"""
@timing
def run():
    [math.cos(x) for x in numbers]

run()

"""Using numpy"""
@timing
def run():
    np.cos(numbers)

run()