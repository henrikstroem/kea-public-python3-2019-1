import functools

from timing import timing


N = 38

def naive_fib(n):
    if n < 2:
        return 1
    return naive_fib(n-2) + naive_fib(n-1)

@timing
def run_naive():
    naive_fib(N)

run_naive()

@functools.lru_cache(maxsize=None)
def fast_fib(n):
    if n < 2:
        return 1
    return fast_fib(n-2) + fast_fib(n-1)

@timing
def run_fast():
    fast_fib(N)
    print(fast_fib.cache_info())

run_fast()