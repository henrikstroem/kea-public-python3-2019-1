import time


def timing(fn):
    def inner_fn():
        t0 = time.time()
        fn()
        t1 = time.time()
        print(f"Time: {t1 - t0:10.4f}")
    return inner_fn