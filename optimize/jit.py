import random
import time

import numba


N = 10_000_000

def monte_carlo_pi(nsamples):
    acc = 0
    for i in range(nsamples):
        x = random.random()
        y = random.random()
        if (x**2 + y**2) < 1.0:
            acc += 1
    return 4.0 * acc / nsamples


@numba.jit
def jit_monte_carlo_pi(nsamples):
    acc = 0
    for i in range(nsamples):
        x = random.random()
        y = random.random()
        if (x**2 + y**2) < 1.0:
            acc += 1
    return 4.0 * acc / nsamples


t0 = time.time()
print(monte_carlo_pi(N))
t1 = time.time()
print(jit_monte_carlo_pi(N))
t2 = time.time()

print(f"Time: {t1 - t0:10.4f}")
print(f"Time: {t2 - t1:10.4f}")
