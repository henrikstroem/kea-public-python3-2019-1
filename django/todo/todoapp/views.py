from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.urls import reverse

from .models import Todo


@login_required
def index(request):
    todos = Todo.objects.filter(user=request.user)
    context = {
        'todos': todos
    }
    return render(request, 'todoapp/index.html', context)


def details(request, pk):
    todo = get_object_or_404(Todo, pk=pk)

    if request.method == 'GET':
        context = {
            'todo': todo
        }
        return render(request, 'todoapp/details.html', context)

    if request.method == 'POST':
        todo.text = request.POST['text']
        completed = request.POST.getlist('completed')
        if len(completed) > 0:
            todo.completed = True
        else:
            todo.completed = False
        todo.save()
        return HttpResponseRedirect(reverse('todoapp:index'))

    return HttpResponseBadRequest()

def new(request):
    if request.method == 'GET':
        return render(request, 'todoapp/new.html')

    if request.method == 'POST':
        todo = Todo()
        todo.user = request.user
        todo.text = request.POST['text']
        completed = request.POST.getlist('completed')
        if len(completed) > 0:
            todo.completed = True
        else:
            todo.completed = False
        todo.save()
        return HttpResponseRedirect(reverse('todoapp:index'))

    return HttpResponseBadRequest()