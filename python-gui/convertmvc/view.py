import tkinter as tk


class CurrencyConverterView:

    def __init__(self, controller):

        self.controller = controller

        self.window = tk.Tk()
        self.window.title("Currency Converter")
        self.window.geometry("400x400")

        self.from_label = tk.Label(self.window, text="From")
        self.from_label.grid(column=0, row=0)

        self.to_label = tk.Label(self.window, text="To")
        self.to_label.grid(column=2, row=0)

        self.amount_label = tk.Label(self.window, text="Amount")
        self.amount_label.grid(column=0, row=1)

        self.converted_amount_label = tk.Label(self.window, text="Converted Amount")
        self.converted_amount_label.grid(column=0, row=2)

        self.converted_result_label = tk.Label(self.window, text="")
        self.converted_result_label.grid(column=1, row=2)

        self.from_ctrl = tk.StringVar(self.window)
        self.from_ctrl.set(self.controller.supported_currencies()[0])
        self.from_opt_menu = tk.OptionMenu(self.window, self.from_ctrl, *self.controller.supported_currencies(), command=self.controller.from_opt)
        self.from_opt_menu.grid(column=1, row=0)

        self.to_ctrl = tk.StringVar(self.window)
        self.to_ctrl.set(self.controller.supported_currencies()[1])
        self.to_opt_menu = tk.OptionMenu(self.window, self.to_ctrl, *self.controller.supported_currencies(), command=self.controller.to_opt)
        self.to_opt_menu.grid(column=3, row=0)

        self.convert_button = tk.Button(self.window, text="Convert", command=self.controller.convert_action)
        self.convert_button.grid(column=0, row=3)

        self.clear_button = tk.Button(self.window, text="Clear", command=self.controller.clear_action)
        self.clear_button.grid(column=1, row=3)

        self.about_button = tk.Button(self.window, text="About", command=self.controller.about_action)
        self.about_button.grid(column=2, row=3)

        self.quit_button = tk.Button(self.window, text="Quit", command=self.controller.quit_action)
        self.quit_button.grid(column=3, row=3)

        self.amount_field = tk.Entry(self.window, width=10, justify='right')
        self.amount_field.insert(tk.END, '1.00')
        self.amount_field.config(highlightbackground='black', highlightthickness=1)
        self.amount_field.grid(column=1, row=1)