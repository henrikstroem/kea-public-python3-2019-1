import tkinter as tk

import convertmvc.view
import convertmvc.model


class CurrencyController:

    def __init__(self):
        self.model = convertmvc.model.CurrencyConverter()
        self.view = convertmvc.view.CurrencyConverterView(self)
        self.from_currency = self.supported_currencies()[0]
        self.to_currency = self.supported_currencies()[1]
        self.view.window.mainloop()

    def supported_currencies(self):
        return self.model.supported_currencies()

    def from_opt(self, currency):
        self.from_currency = currency

    def to_opt(self, currency):
        self.to_currency = currency

    def convert_action(self):
        amount = float(self.view.amount_field.get())
        self.view.converted_result_label.config(text=str(self.model.convert(self.from_currency, self.to_currency, amount)))

    def clear_action(self):
        self.view.amount_field.delete(0, tk.END)
        self.view.amount_field.insert(tk.END, "1.00")
        self.view.converted_result_label.config(text="")

    def about_action(self):
        pass

    def quit_action(self):
        self.view.window.destroy()
