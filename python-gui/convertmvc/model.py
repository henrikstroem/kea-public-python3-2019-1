import json

import requests


class CurrencyConverter:

    def __init__(self):
        with open('.currencies.json') as currency_file:
            self.currencies = json.load(currency_file)
        with open('.config.json') as config_file:
            self.config = json.load(config_file)
    
    def convert(self, currency_in, currency_out, amount=1.0):
        apikey = self.config["apikey"]
        querystring = self.config["querystring"].format(**locals())
        request = requests.get(querystring)
        return json.loads(request.content)[f"{currency_in}_{currency_out}"]["val"] * amount

    def supported_currencies(self):
        return list(self.currencies.keys())
        