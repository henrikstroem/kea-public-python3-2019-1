# Type hinting.


def shorten(x: str) -> str:
    """Shortens to 3 characters."""
    return x[:3]


print(shorten("Hello, world"))


def shorten2(x):
    """Takes a string x, and returns a shortened version of the string."""
    return x[:3]


print(shorten2("Hey dude"))
