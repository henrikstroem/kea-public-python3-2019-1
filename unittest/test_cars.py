import unittest

from cars.hatchbacks import Hatchback


class TestHatchback(unittest.TestCase):

    def setUp(self):
        self.car = Hatchback(top_speed=168)

    def tearDown(self):
        print("This is tearDown.")

    def test_top_speed(self):
        self.assertEqual(self.car.top_speed, 168)
        self.car.top_speed = 172
        self.assertEqual(self.car.top_speed, 172)